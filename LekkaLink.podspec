Pod::Spec.new do |spec|
  spec.name         = 'LekkaLink'
  spec.version      = '1.0'
  spec.license      = 'MIT'
  spec.summary      = 'LekkaLink is a Cocoa framework to automatically capture screenshots'
  spec.homepage     = 'https://gitlab.com/lekka/lekka-link-ios'
  spec.author       = 'Peeka Sharma'
  spec.source       = { :git => 'https://gitlab.com/lekka/lekka-link-ios.git', :tag => '1.0' }
  spec.requires_arc = true
  spec.dependency 'Alamofire', '~> 5.0.5'
  spec.dependency 'SVProgressHUD'
  spec.dependency 'ReachabilitySwift'
  spec.dependency 'DeviceKit', '~> 2.0'
  spec.static_framework = true
  spec.ios.deployment_target = '10.0'
  spec.swift_version = "5"
  spec.xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
  spec.source_files = 'LekkaLink/**/*.{h,m,swift}'
  spec.xcconfig  =  { 'LIBRARY_SEARCH_PATHS' => '$(PROJECT_DIR)/Pods/LekkaLink/Library'}
  spec.preserve_paths = 'Library/*'

end
