import Foundation
import UIKit
import DeviceKit
//
//  LekkaLink.swift
//  LekkaLink
//
//  Created by Peeka on 04/03/20.
//  Copyright © 2020 Lekka. All rights reserved.
//

 public class LekkaLink
 {
      var screenListArray = NSArray()
    
    
    
    public init()
    {
    }
    
    //Public Method to set Access Toke and Project ID
    public func setAccessTokenforProject(accessToken:NSString, projectId:NSString)
    {
        if accessToken == ""
        {
            print("access token should not be blank")
        }
        if projectId == ""
        {
            print("project id should not be blank")
        }
        let defaults = UserDefaults.standard
        ObjcKeys.accessToken = accessToken as String
        ObjcKeys.projectId = projectId as String
        defaults.set(accessToken, forKey:"accessToken")
        defaults.set(projectId, forKey:"projectId")
    }
    
    
    //Public Method to Capture Screenshot
    
    public func captureScreenshot(view: UIView, eventName:NSString)
    {
       if Utils.Is_Internet_Connection_Available()
       {
           let image = self.takeScreenshot(view: view)
              let success =  self.saveImagetoDocumentDirectory(eventName: eventName, image: image)
              print(success)
               if success
               {
                    self.checkScreenOnServer(evetName: eventName)
                    {
                        var screenId = ""
                        var isScreenExist = false
                        for item in self.screenListArray
                        {
                                                  
                                let dict = item as! NSDictionary
                                let screenName = dict.value(forKey: "value") as! NSString
                                                 if screenName == eventName
                                                  {
                                                    isScreenExist = true
                                                    screenId = dict.value(forKey: "id") as! String
                                                       break
                                                  }
                                                 
                                              }
                                             if isScreenExist
                                              {
                                                 print("id is===\(screenId)")
                                                self.fnPostScreenshot(screenId:screenId as NSString, eventName: eventName)
                                              }
                                              else
                                              {
                                                   self.fnCreateScreen(screenshot: image, eventName: eventName)
                                              }
                    
                    }
               }
                
                
        }
        else
        {
            self.displayAlert(message: Constants.ALERT_CHECK_INTERNET)
        }
      
    }
    
    // Method to capture screenshot of a view
    
    func takeScreenshot(view: UIView) -> UIImage
    {
           // Begin context
           UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
      
           // Draw view in that context
             view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)

           // And finally, get image
           let screenShot = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
           
        let  image = screenShot!

        return image
       }
    
    // Method to show alerts
    
    func showAlert(msg :String,titleMessage: String)
    {
        let iAlertController = UIAlertController(title:msg, message: titleMessage , preferredStyle: .alert)
        
        let iDefaultAction = UIAlertAction(title: NSLocalizedString("A_OK", comment: ""), style: .default, handler: nil)
        iAlertController.addAction(iDefaultAction)
    }
    
    // Method to find pixel density
    
     func getPixelDensity() -> Int
    {
        let device = Device.current
        var pixelDensity = 1 as Int
        
        
        print(device)     // prints, for example, "iPhone 6 Plus"
        
        if device == .iPhoneXSMax || device == .iPhoneXS || device == .iPhoneX || device == .iPhone8Plus || device == .iPhone7Plus || device == .iPhone6Plus || device == .iPhone6sPlus {
             pixelDensity = 3
           } else
           {
              pixelDensity = 2
           }
         print("pixel density is \(pixelDensity)")
           return pixelDensity
       }
    
    // API call to get all screens of project
    
    func checkScreenOnServer(evetName: NSString,completion: (() -> Swift.Void)? = nil)
    {
        let itoken = ObjcKeys.accessToken
        let baseUrl = ObjcKeys.baseAPIURL
        let projectId = ObjcKeys.projectId
        let headers = [
                     "Content-Type": "application/json",
                     "apikey" : itoken
                 ]
        let url =  baseUrl + "Projects/\(projectId)/screens"
        WebServiceManager.callWebService(WSUrl: url, WSMethod: .get, WSParams:[:], WSHeader: headers, WSCompletionBlock: { (iData, iError) in
                
                                      if let error = iError {

                                          print("aErrorObj===\(error.description)")
                                          print("no data found")

                                      }  else {

                                          if let iArrayResponse = iData as? NSArray
                                          {
                                            self.screenListArray = iArrayResponse
                                            completion!()
            
                                                
                                              print("resonse===\(iArrayResponse)")
                                          }
            }}
                  )
    }
    
    // API call to create screen on server
    
    func fnCreateScreen(screenshot :UIImage, eventName:NSString)
       {
           let itoken = ObjcKeys.accessToken
           let projectId = ObjcKeys.projectId
           let baseUrl = ObjcKeys.baseAPIURL
           let positionDict = [
               "x": 100,
               "y": 100
            ] as [String : Any]
        
        let pixelDensity = self.getPixelDensity()
           let url =  baseUrl + "Projects/\(projectId)/screens"
           let headers = [
               "Content-Type": "application/json",
               "apikey" : itoken
           ]
           let parameters = [
               "image": "image.jpg",
               "value": eventName,
               "pixelDensity": pixelDensity,
               "linkedScreens": [],
               "attributes": [],
               "position": positionDict
           ] as [String : Any]
        
           WebServiceManager.callWebService(WSUrl: url, WSMethod: .post, WSParams: parameters as Dictionary<String, AnyObject>, WSHeader: headers, WSCompletionBlock: { (iData, iError) in
                           if let screenId = iData as? NSString
                           {
                            let id = screenId.replacingOccurrences(of: "\"", with: "")
                            
                            self.fnPostScreenshot(screenId: id as NSString, eventName: eventName as NSString)

                           }  else {
               
                               if let error = iError
                               {
                                   print("aErrorObj===\(error.description)")
                                   print("no data found")
               
                               }  else
                               {
                                    
                               }
            }
        }
           )
       }
    
    // API call to Post Screenshot
    
    func fnPostScreenshot(screenId:NSString, eventName: NSString)
    {
        let imageName = (eventName as String)+".png"
        let filePath  = self.getImage(imageName:imageName as String)
    //    self.loadImageFromDocumentDirectory(nameOfImage: eventName as String)
        print(filePath)
        let itoken = ObjcKeys.accessToken
        let baseUrl = ObjcKeys.baseAPIURL

        let url =  baseUrl + "screenelements/\(screenId)/images"

        let headers = [
            "content-type": "multipart/form-data",
            "apikey" : itoken
        ]
        let parameters = [:] as [String : Any]

        WebServiceManager.callImageUploadWithParameterUsingMultipart(url: url, filePath: filePath, params: parameters, header: headers,completion: { (iData, iError) in
                        if let _ = iData as? String {

                        }  else {

                            if let error = iError {

                                print("aErrorObj===\(error.description)")
                                print("no data found")

                            }  else {

                                if let iDictResponse = iData as? NSDictionary {

                                    print("resonse===\(iDictResponse)")
                                    let screenshotName = (eventName as String)+".png"
                                    self.removeImage(imageName: screenshotName)

                                    }  else {

                                    }

                                }
                            }
                        }
        )
    }
    
    //Method to save image temporaraly in document directory
    
    func saveImagetoDocumentDirectory(eventName:NSString, image:(UIImage)) -> Bool
    {
        let imageName = (eventName as String)+".png"
        // get the documents directory url
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        // choose a name for your image
        let filename = imageName as NSString
        print(filename)
        // create the destination file url to save your image
        let fileURL = documentsDirectory.appendingPathComponent(filename as String)

        let compressionValue = self.getImageSize(image: image)
        let data = image.jpegData(compressionQuality: CGFloat(compressionValue))
        
        FileManager.default.createFile(atPath: fileURL.path as String, contents: data, attributes: nil)
        // get your UIImage jpeg data representation and check if the destination file url already exists
//        if let data = image.jpegData(compressionQuality: CGFloat(compressionValue)),
//          !FileManager.default.fileExists(atPath: fileURL.path) {
//            do {
//                // writes the image data to disk
//                try data.write(to: fileURL)
//                print("file saved")
//                return true
//            } catch {
//                print("error saving file:", error)
//                return true
//            }
//        }
        return true
    }
    
    //Method to get image size

     func getImageSize(image:UIImage) -> Float
         {
              let imageData =  image.jpegData(compressionQuality:  CGFloat(1))
              let imageSize = imageData!.count
              let imageSizeInMb = imageSize/1024/1024 as Int
              var compressionValue = 1.0
             
             if imageSizeInMb >= 1
             {
                 let newImageData = image.jpegData(compressionQuality:  CGFloat(0.8))
                 let newImageSizeInMb = (newImageData!.count)/1024/1024 as Int
                 if newImageSizeInMb >= 1
                 {
                     compressionValue =  0.5
                    return Float(compressionValue)
                 }
                 else
                 {
                     compressionValue = 0.8
                    return Float(compressionValue)
                 }
             }
             print("Size of Image: \(imageSize/1024/1024) mb")
             print("image size is  file:", imageSize)
            return Float(compressionValue)
                   
         }
    func getDirectoryPath() -> String
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    // Method to get screenshot image from document directory
    
    func getImage(imageName : String)-> NSString
    {
            let fileManager = FileManager.default
            let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
            if fileManager.fileExists(atPath: imagePath){
                return imagePath as NSString
            }
            else
            {
                print("No Image available")
                return ""
            }
    }
    
    //method to remove image after uploading it to server
    
    func removeImage(imageName: String)
    {
        let fileManager = FileManager.default
        let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        var success = false
        do
        {
            try fileManager.removeItem(atPath: imagePath)
            success = true
        }
        catch {
        }
        if success
        {
            print("image deleted succcessfully")
        }
        else {
        }
    }

    func displayAlert(message:String)
    {
        let alert = UIAlertController(title: "", message:message, preferredStyle: UIAlertController.Style.alert)
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        // show alert
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
}

