//
//  Constants.swift

//
//  Created by Prince Sojitra on 07/11/16.
//  Copyright © 2016 Prince Sojitra. All rights reserved.
//

import UIKit
import Alamofire
import Reachability

public class ObjcKeys : NSObject
{
    
    class var swiftSharedInstance: ObjcKeys {
        struct Singleton {
            static let instance = ObjcKeys()
        }
        return Singleton.instance
    }
    static let baseAPIURL = "https://lekka.herokuapp.com/api/" //Live URL
    static var accessToken = ""
    static var projectId = ""
}
class Constants
{
    // MARK: - GENERAL CONSTANT
    
    static let DEVICE_Opacity = "DeviceOpacity"
    
    
    // MARK: - ALERT CONSTANTS
    
    static let ALERT_CHECK_INTERNET = "No internet connection availiable.Please try again later"
    
}
    
public struct Utils
{
    // Method to check Internet connectivity
    
    static func Is_Internet_Connection_Available() -> Bool {
        
        
        let reachability = Reachability.init()
        let internetStatus = reachability?.currentReachabilityStatus
        
        if internetStatus == .notReachable {
           
            return false
        }
        else {
        
            return true
        }
    }
}


