//
//  TOD_WebServiceManager.swift
//  TOD
//
//  Created by Prince Sojitra on 06/09/16.
//  Copyright © 2016 DRC Systems. All rights reserved.
//

import Foundation
import Alamofire


class WebServiceManager: NSObject
{

    static var instance: WebServiceManager!

    var isLoaderRemoved:Bool = true
    var timer:Timer?
    class func sharedInstance() -> WebServiceManager
    {
        self.instance = (self.instance ?? WebServiceManager())
        return self.instance
    }
    
    
    class func callWebService(WSUrl:String, WSMethod:HTTPMethod,WSParams:Dictionary<String, AnyObject> ,WSHeader :[String:String],isLoader:Bool = true,WSCompletionBlock:@escaping (_ data:AnyObject?,_ error:NSError?) -> ())
       {
           if Utils.Is_Internet_Connection_Available() {
            let iHeaders = HTTPHeaders(WSHeader)
            
            if WSMethod ==  .get
            {
                AF.request(WSUrl, method: .get, parameters:nil, encoding:JSONEncoding.default, headers: iHeaders).responseString { (response) in
                print(response.response?.statusCode ?? "not found")
                switch response.result {
                     case .success(_):
                     print("success")
                        if response.value != nil
                        {
                                                 let responseData = response.data! as NSData
                                                 
                                                 let json = NSString(data: responseData as Data , encoding: String.Encoding.utf8.rawValue)
                                                                                    
                                                     let newString = json!.replacingOccurrences(of: "<null>", with: "")
                                                                                    
                                                     if let data = newString.data(using: String.Encoding.utf8) {
                                                     do {
                                                            
                                                             let  iDictResponse = try JSONSerialization.jsonObject(with: data, options:.mutableLeaves) as! NSArray
                                                             WSCompletionBlock(iDictResponse as AnyObject,nil)
                                                             }
                                                             catch let error as NSError
                                                             {
                                                                    WSCompletionBlock(nil,error as NSError)
                                                             }
                                                         }
                        }
                     break
                     case .failure(let error):
                        switch error.responseCode {
                        case 401:
                            print("access token is invalid")
                            break
                        default: break

                        }
                          WSCompletionBlock(nil,error as NSError)
                      break
                     }
                 }
            }
            else
            {
                AF.request(WSUrl, method: .post, parameters:WSParams, encoding:JSONEncoding.default, headers: iHeaders).responseString { (response) in
                print(response.error?.responseCode ?? "not found")
                switch response.result {
                     case .success(_):
                     print("success")
                       if response.value != nil
                        {
                           WSCompletionBlock(response.value as AnyObject,nil)
                        }
                     break
                     case .failure(let error):
                          print(error)
                          WSCompletionBlock(nil,error as NSError)
                      break
                     }
                 }
            }

            
        }
    }
    
    class func callImageUploadWithParameterUsingMultipart( url: String, filePath: NSString, params: [String : Any], header: [String:String], completion:@escaping (_ data:AnyObject?,_ error:NSError?) -> ())
    {
         if Utils.Is_Internet_Connection_Available()
         {
            
                   let iHeaders = HTTPHeaders(header)
                   let fileUrl = URL(fileURLWithPath: filePath as String)
                   AF.upload(multipartFormData: { multiPart in
                       multiPart.append(fileUrl, withName: "file")
                   }, to: url, method: .post, headers: iHeaders) .uploadProgress(queue: .main, closure: { progress in
                       print("Upload Progress: \(progress.fractionCompleted)")
                   }).responseJSON { response in
                                   print(response)
                                   if let status = response.response?.statusCode {
                                       switch(status){
                                       case 200:
                                           if (response.value != nil)
                                             {
                                               let responseData = response.data! as NSData
                                               
                                               let json = NSString(data: responseData as Data , encoding: String.Encoding.utf8.rawValue)
                                                                                  
                                                   let newString = json!.replacingOccurrences(of: "<null>", with: "")
                                                                                  
                                                   if let data = newString.data(using: String.Encoding.utf8) {
                                                   do {
                                                           let  iDictResponse = try JSONSerialization.jsonObject(with: data, options:.mutableLeaves) as! Dictionary<String, AnyObject>
                                                           completion(iDictResponse as AnyObject,nil)
                                                           }
                                                           catch let error as NSError
                                                           {
                                                                  completion(nil,error as NSError)
                                                           }
                                                       }
                                                  
                                               }
                                       default:
                                           print("error with response status: \(status)")
                                       }
                                   }

                           }
               }
            
        }

}

