# LekkaLink #

LekkaLink is a Cocoa framework to automatically capture screenshots of different view of an application and upload it to Lekka Server.

## Versions:

Latest version of LekkaLink library is 1.0.1


# Getting Started

## Installation(add LekkaLink to your app)

You have two main options for installing LekkaLink:

1. Use CocoaPods
2. Drag/drop the static library into your project, and configure the build settings in your project

## Option 1: Using CocoaPods

[CocoaPods](https://github.com/CocoaPods/CocoaPods) is a dependency manager for CocoaTouch. To do CocoaPods, adding the following in your podfile:

```
pod 'LekkaLink', '~> 1.0.1'
```

## Option 2: Drag/drop the static library 

1. Download code and open Library folder. Copy libLekkaLink.a file and  LekkaLink.swiftmodule folder.

2. Create a lib folder in your project and paste  libLekkaLink.a file and  LekkaLink.swiftmodule folder in the lib folder.

   <p align="center"><img src="Screen01.jpg" /></p>

3. Add  libLekkaLink.a file and LekkaLink.swiftmodule into your projects.

4. Select the project’s name in project navigator, then select General, and select your’s application target.The section Linked Frameworks and Libraries has to contain a line with libLekkaLink.a. If it does not, press the + button and select it manually. Make sure that the Required status is selected.
    
   <p align="center"><img src="Screen03.jpg" /></p>

5. After that, go to the Build Phases tab, expand Link Binary with Libraries, and make sure that it contains a line with libLekkaLink.a. If it does not, again, add it manually and set it to required.
  
   <p align="center"><img src="Screen04.jpg" /></p>

6. In the Build Settings tab, select your target , select All , and Combined .In the search field in the top-right corner, type Search Paths. Copy the path from Library Search Path and paste it to Import Paths . If there is no value in Library Search Path, add a new one, $(PROJECT_DIR)/lib. Do the same for Import Paths.

   <p align="center"><img src="Screen05.jpg" /></p>
   
   <p align="center"><img src="Screen06.jpg" /></p>

7. Add frameworks

   * 'Alamofire', '~> 5.0.5'
   * 'SVProgressHUD'
   * 'ReachabilitySwift'
   * 'DeviceKit', '~> 2.0'
  
 
    <p align="center"><img src="Screen07.jpg" /></p>


## Usage


 ### 1. Import LekkaLink library. It will give you the ability to use the code from your library in ViewController.
 
    import LekkaLink 
 
 ### 2. Create an instance of a class that belongs to the library.
 
    let lekkaLink  = LekkaLink() 
 
  ### 3.  Set Access Token and project Id  for the application using method below:  
 
    lekkaLink.setAccessTokenforProject(accessToken: "<Access-Token>", projectId:"<Project-Id>")
    
    Access Token - Access Token created on the Lekka Web.
    Project-Id - Unique Id of the project to which you want to Post screenshot.
    
   ### 4. Capture screenshot
 
  
      lekkaLink.captureScreenshot(view:<UIView>, eventName: "<EventName>")
     
      UIView - any view for which you want to capture screenshot.
      EventName - Event name is the unique name of the screen to which screenshot image will be uploaded.
     
      Naming convention for event Name:  App-{{AppPlattform}}.{{epic}}.{{component}}.{{state}}
     
      App -  Name of the application
      AppPlatform - Application platform
      epic - Name of the screen 
      component - Name of the component you want to capture screenshot.
      state - Name of the event on which screenshot is captured/
      
      For example  "testApp-ios.login.login.default"
      
      Note: You need to call set access token and project id method before capturing screenshot.
      
      
# Example
    
import UIKit
import LekkaLink 

class ViewController: UIViewController 
{

    let lekkaLink  = LekkaLink() 
    
    override func viewDidLoad() 
    {
        super.viewDidLoad()
        lekkaLink.setAccessTokenforProject(accessToken: "<Access-Token>", projectId:"<Project-Id>")
        
        lekkaLink.captureScreenshot(view:<UIView>, eventName: "String")

    }
    
}

